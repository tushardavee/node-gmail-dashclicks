const {google} = require('googleapis');
var base64 = require('js-base64').Base64;
const cheerio = require('cheerio');


class Check{
    //auth is the constructor parameter.
    constructor(auth){
        this.me = 'tusharsemails@gmail.com';
        this.gmail = google.gmail({version: 'v1', auth});
        this.auth = auth;
    }

    //Returns the mails in the user's inbox.
    checkInbox(){
        this.gmail.users.messages.list({
            userId: this.me
        }, (err, res) => {
            if(!err){
                console.log(res.data);
            }
            else{
                console.log(err);
            }
        })
    }

    //We can attatch a query parameter to the request body.
      checkForLatestEmails(){
      //  var query = "from:no-reply@accounts.google.com";
       // We need only latest 10 emails
        this.gmail.users.messages.list({
            userId: this.me,
           // q: query,// We can query for specific options like unread and from particular user etc,but not required in this case
            maxResults: 10, // Tushar => We need only latest 10 emails
        }, (err, res) => {
            if(!err){

                //mail array stores the mails.
                var mails = res.data.messages;
               // console.log(mails);    
                //We call the getMail function passing the id of first mail as parameter.
             this.getMail(mails[0].id);
            }
            else{
                console.log(err);
            }
        });        
    }

    //getMail function retrieves the mail body and parses it for useful content.
    //In our case it will parse for all the links in the mail.
    getMail(msgId){
        //This api call will fetch the mailbody.
        this.gmail.users.messages.get({
            'userId': this.me,
            'id': msgId
        },  (err, res) => {
            if(!err){
                var body = res.data.payload.parts[0].body.data;
                var htmlBody = base64.decode(body.replace(/-/g, '+').replace(/_/g, '/'));
                //console.log(res.data.payload.headers);
                // Tushar => convert the received object to array.
                // and use the Array.filter method to find the sender email id.
                let fromObject=Object.values(res.data.payload.headers).filter((val)=>{
                    if(val.name==='From'){
                        return val;
                    }
                })
                console.log("sender email is: "+fromObject[0].value);
                console.log("sender body is: "+htmlBody);
            }
        });
    }
}
module.exports = Check;